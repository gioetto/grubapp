import { API } from "./util";

function FormInput({
  students,
  setStudents,
  id,
  setId,
  name,
  setName,
  email,
  setEmail,
  dateOfBirth,
  setDateOfBirth,
}) {
  return (
    <form onSubmit={addStudent}>
      <label>Id</label>
      <input
        required
        type="text"
        name="id"
        value={id}
        placeholder="Id"
        onChange={(e) => setId(e.target.value)}
      />
      <br />
      <label>Name</label>
      <input
        value={name}
        type="text"
        name="name"
        placeholder="Name"
        onChange={(e) => setName(e.target.value)}
      />
      <br />
      <label>Email </label>
      <input
        value={email}
        type="text"
        name="email"
        placeholder="email"
        onChange={(e) => setEmail(e.target.value)}
      />
      <br />
      <label>Date of Birth</label>
      <input
        value={dateOfBirth}
        type="text"
        name="dateOfBirth"
        placeholder="Date of Birth"
        onChange={(e) => setDateOfBirth(e.target.value)}
      />
      <br />
      <input type="submit" value="Submit" />
    </form>
  );

  function addStudent(event) {
    event.preventDefault();

    const newStudent = { id, name, email, dateOfBirth };
    if (students.map((s) => s.id).includes(id)) {
      let newStudents = [...students];
      newStudents = newStudents.map((s) => {
        if (s.id === id) return newStudent;
        else return s;
      });
      setStudents(newStudents);
    } else {
      setStudents([...students, newStudent]);

      console.log(`Added new student ${newStudent}`);
    }

    const url = `${API}/students`;

    fetch(url, {
      method: "PUT",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newStudent),
    })
      .then((response) => response.json())
      .then((response) => {
        console.log(response);
      });

    clearInputs();
  }

  function clearInputs() {
    setId("");
    setName("");
    setEmail("");
    setDateOfBirth("");
  }
}

export default FormInput;

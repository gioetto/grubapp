import { API } from "./util";

function Table({
  students,
  setStudents,
  setId,
  setName,
  setEmail,
  setDateOfBirth,
}) {
  return (
    <table>
      <thead>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Email</th>
          <th>Date of Birth</th>
        </tr>
      </thead>
      <tbody>
        {students.map((student) => (
          <tr key={student.id}>
            <td>{student.id}</td>
            <td>{student.name}</td>
            <td>{student.email}</td>
            <td>{student.dateOfBirth}</td>
            <td>
              <button onClick={() => editStudent(student)}>Edit</button>
            </td>
            <td>
              <button onClick={() => deleteStudent(student.id)}>Delete</button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );

  function editStudent(student) {
    console.log(student);
    setId(student.id);
    setName(student.name);
    setEmail(student.email);
    setDateOfBirth(student.dateOfBirth);
  }

  function deleteStudent(id) {
    const newStudents = students.filter((s) => s.id !== id);
    setStudents(newStudents);

    const url = `${API}/students/${id}`;

    fetch(url, {
      method: "DELETE",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((response) => {
        console.log(response);
      });
  }
}

export default Table;

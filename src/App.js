import "./App.css";
import { useState, useEffect } from "react";
import { API } from "./util";
import Table from "./table";
import FormInput from "./formInput";

function App() {
  const [students, setStudents] = useState([]);
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [dateOfBirth, setDateOfBirth] = useState("");

  useEffect(() => {
    console.log(API);

    fetch(`${API}/students`)
      .then((response) => response.json())
      .then((data) => {
        setStudents(data.Items);
      });
  }, []);

  return (
    <div className="App">
      <h2>Students</h2>
      <Table
        setId={setId}
        setName={setName}
        setEmail={setEmail}
        setDateOfBirth={setDateOfBirth}
        students={students}
        setStudents={setStudents}
      ></Table>
      <h5>Add/Edit students</h5>
      <FormInput
        id={id}
        setId={setId}
        name={name}
        setName={setName}
        email={email}
        setEmail={setEmail}
        dateOfBirth={dateOfBirth}
        setDateOfBirth={setDateOfBirth}
        students={students}
        setStudents={setStudents}
      ></FormInput>
    </div>
  );
}

export default App;
